 /*---slider activation---*/
 $('.slider_area').owlCarousel({
    animateOut: 'fadeOut',
    autoplay: true,
    loop: true,
    nav: false,
    autoplayTimeout: 1000 * 5,
    items: 1,
    dots: true,
});
