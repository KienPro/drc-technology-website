
     /*---blog column4 activation---*/
     $('.blog_column4').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplayTimeout: 8000,
        items: 4,
        dots:false,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            768:{
				items:3,
			},
             992:{
				items:3,
			},
            1200:{
				items:4,
			},
		  }
    });

    /*---blog column1 activation---*/
    $('.blog_column1').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 1,
        dots:false,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            768:{
				items:3,
                margin:30,
			},
             992:{
				items:1,
			},
		  }
    });

    /*---blog thumb activation---*/
    $('.blog_thumb_active').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 1,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
    });
