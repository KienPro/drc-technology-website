 /*---portfolio Isotope activation---*/
 $('.portfolio_gallery').imagesLoaded( function() {

    var $grid = $('.portfolio_gallery').isotope({
       itemSelector: '.gird_item',
        percentPosition: true,
        masonry: {
            columnWidth: '.gird_item'
        }
    });

      /*---ilter items on button click---*/
    $('.portfolio_button').on( 'click', 'button', function() {
       var filterValue = $(this).attr('data-filter');
       $grid.isotope({ filter: filterValue });
        
       $(this).siblings('.active').removeClass('active');
       $(this).addClass('active');
    });
   
});