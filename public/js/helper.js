var showLoading = function () {
    $('body').loading({
        zIndex : 10000
    });
};

var hideLoading = function () {
    $('body').loading('stop');
};

var setToastMessage = function (message) {
    localStorage.setItem('action', JSON.stringify({
        success: true,
        message: message
    }));
};

var showToastSuccess = function (message) {
    $.toast({
        heading: 'Success',
        text: message,
        showHideTransition: 'plain',
        allowToastClose: false,
        position: 'top-right',
        icon: 'success'
    })
};

var showToastWarning = function (message) {
    $.toast({
        heading: 'Warning',
        text: message,
        showHideTransition: 'plain',
        allowToastClose: false,
        position: 'top-right',
        icon: 'info'
    })
};

var showToastError = function (message) {
    $.toast({
        heading: 'Error',
        text: message,
        showHideTransition: 'slide',
        showHideTransition: 'plain',
        allowToastClose: false,
        position: 'top-right',
        icon: 'error'
    })
};

$(function () {
    let alert = localStorage.getItem('action');
    if (alert) {
        alert = JSON.parse(alert);
        if (alert.success == true) {
            $.toast({
                heading: 'Success',
                text: alert.message,
                showHideTransition: 'slide',
                icon: 'success'
            })
        }
        localStorage.setItem('action', '');
    }
});

var showAlertError = function (messages) {
    let error_message = messages;
    if(Array.isArray(messages) || typeof(messages) === 'object') {
        error_message = '';
        for (let key in messages) {
            error_message = error_message + messages[key] + '<br>';
        }
    }
    error_message = error_message ? error_message : "Something went wrong";
    $.alert({
        title: 'Message!',
        content: error_message,
        type: 'orange',
        typeAnimated: true,
        escapeKey: 'close',
        buttons: {
            close: function () {
            }
        }
    });
};

function defaultStatus(id, state) {
    $('#status-' + id).prop("checked", state);
}

function showErrorTitle(messages, title, that) {
    var error_message = '';
    if (messages.title_en_exist) {
        error_message = 'The English '+ title +' has already been taken.'
    }

    if (messages.title_kh_exist) {
        error_message = (error_message ? '<br>' : '') + 'The Khmer '+ title +' has already been taken.';
    }

    $.confirm({
        title: 'Duplicate ' + title,
        content: error_message + '<br>Are you sure you want to duplicate ' + title + '?',
        type: 'orange',
        typeAnimated: true,
        buttons: {
            yes: {
                text: 'Yes',
                btnClass: 'btn-orange',
                action: function(){
                    that.save();
                }
            },
            close: function () {
                hideLoading();
            }
        }
    });
}
