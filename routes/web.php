<?php

// Home
Route::get('/', 'HomeController@index')->name('home');
Route::group(['prefix' => 'home'], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/product', 'HomeController@product')->name('product');
    Route::get('/product/compare/{id}', 'HomeController@compare')->name('product.compare');

    Route::get('/product-details/{id}', 'HomeController@productDetails')->name('product-details');
    Route::get('/quickview/{id}', 'HomeController@quickView')->name('quickview');
});

//Shop
Route::get('/shop', 'ShopController@index')->name('shop');

//Compare
Route::get('/compare', 'CompareController@index')->name('compare');

//Service
Route::get('/service', 'ServiceController@index')->name('service');

//About
Route::get('/about', 'AboutController@index')->name('about');

//Contact
Route::get('/contact', 'ContactController@index')->name('contact');




//Authentication
Route::group(['prefix' => 'auth'], function(){
    //Register
    Route::get('/register', 'AuthController@registerIndex')->name('auth.register.index');
    Route::post('/register', 'AuthController@register')->name('auth.register.register');

    //Login
    Route::get('/login', 'AuthController@loginIndex')->name('auth.login.index');
    Route::post('/login', 'AuthController@login')->name('auth.login.login');

    Route::get('/forget', 'AuthController@forget')->name('auth.forget');
    Route::post('/forget', 'AuthController@submitforgetPassword')->name('auth.forget.submit');

    Route::get('/verify/{email}', 'AuthController@verify')->name('auth.verify');
    Route::post('/verify', 'AuthController@submitVerify')->name('auth.verify.submit');

    Route::get('/reset/{email}/{token}', 'AuthController@reset')->name('auth.reset');
    Route::post('/reset', 'AuthController@submitResetPassword')->name('auth.reset.submit');
});

Route::group(['middleware' => 'auth'], function(){
    //Wishlist
    Route::group(['prefix' => 'wishlists'], function(){
        Route::get('/', 'WishlistController@index')->name('wishlists');
        Route::post('/wish', 'WishlistController@wish')->name('wishlists.wish');
        Route::delete('/unwish/{id}', 'WishlistController@unwish')->name('wishlists.unwish');
    });

    //Cart
    Route::group(['prefix' => 'carts'], function(){
        Route::get('/', 'CartController@index')->name('view-carts');
        Route::get('/item', 'CartController@cartData')->name('get-carts');
        Route::post('/add/single_cart', 'CartController@singleCart')->name('carts.add');
        Route::post('/add/multiple_cart', 'CartController@multipleCart')->name('carts.addToMultiple');
        Route::post('/remove/{id}', 'CartController@delete')->name('carts.delete');
    });

    //Order
    Route::group(['prefix' => 'order'], function(){
        Route::post('/', 'OrderController@store')->name('order.create');
    });

    Route::group(['prefix' => 'profile'], function(){
        Route::get('/', 'ProfileController@edit')->name('profile');
        Route::post('/', 'ProfileController@update')->name('profile.update');
        Route::post('/change-password', 'ProfileController@changepassword')->name('profile.updatepassword');
    });
});
Route::get('/logout', 'AuthController@logout')->name('logout');