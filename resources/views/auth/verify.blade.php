@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('home') }}">home</a></li>
                        <li>Verify Code</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="customer_login mt-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="account_form">
                    <h2>Confirm code</h2>
                    <h4>The code have been sent to: <a href="mailto:".request('email')>{{ request('email') }}</a></h4>
                    <form action="{{ route('auth.verify.submit') }}" method="POST">
                        @csrf
                        <p><label>Code <span class="text-danger">*</span></label>
                            <input type="hidden" name="email" value="{{ request('email') }}">
                            <input type="text" name="verify_code">
                        </p>
                        <div class="login_submit">
                            <button type="submit">Next</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection