@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('home') }}">home</a></li>
                        <li>Forget Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="customer_login mt-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="account_form">
                    <h2>Please enter your email</h2>
                    <form action="{{ route('auth.forget.submit') }}" method="POST">
                        @csrf
                        @php $error = session()->get('error'); @endphp
                        <p><label>email <span class="text-danger">*</span></label><input type="email" name="email"></p>
                        <div class="login_submit">
                            <button type="submit">Next</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</di
@endsection