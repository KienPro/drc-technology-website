@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('home') }}">home</a></li>
                        <li>New Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="customer_login mt-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="account_form">
                    <h2>Create new password</h2>
                    <form action="{{ route('auth.reset.submit') }}" method="POST">
                        @csrf
                        <input type="hidden" name="email" value="{{ request('email') }}">
                        <input type="hidden" name="token" value="{{ request('token') }}">
                        <p>
                            <label>New Passwords <span class="text-danger">*</span></label>
                            <input type="password" name="password">
                        </p>
                        <p>
                            <label>Confirm Passwords <span class="text-danger">*</span></label>
                            <input type="password" name="confirm_password">
                        </p>
                        <div class="login_submit">
                            <button type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection