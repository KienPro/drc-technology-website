@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('home') }}">home</a></li>
                        <li>Login</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="customer_login mt-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="account_form">
                    <h2>login</h2>
                    <form action="{{ route('auth.login.login') }}" method="POST">
                        @csrf
                        @php $error = session()->get('error'); @endphp
                        <p>
                            <label>Email <span class="text-danger">*</span></label>
                            <input type="text" name="email" class="form-control {{ isset($error['val']['email']) ? 'is-invalid' : '' }}" value="{{ old('email') }}">
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $error['val']['email'] ?? ''  }}</strong>
                            </span>
                        </p>
                        
                        <p>
                            <label>Password<span class="text-danger">*</span></label>
                            <input type="password" name="password" class="form-control {{ isset($error['val']['password']) ? 'is-invalid' : '' }}">
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $error['val']['password'] ?? ''  }}</strong>
                            </span>
                        </p>
                        <div class="login_submit">
                            <a href="{{ route('auth.forget') }}">Lost your password?</a>
                            {{-- <label for="remember"><input id="remember" type="checkbox"> Remember me</label> --}}
                            <button type="submit">login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection