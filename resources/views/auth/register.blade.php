@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('home') }}">home</a></li>
                        <li>Register</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="customer_login mt-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="account_form register">
                    <h2>Register</h2>
                    <form action="{{ route('auth.register.register') }}" method="post">
                        @csrf
                        <p>
                            <label>Full Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" value="{{ old('name') }}" required>
                        </p>
                        <p>
                            <label>Email address <span class="text-danger">*</span></label>
                            <input type="email" name="email" value="{{ old('email') }}" required>
                        </p>
                        <p>
                            <label>Phone Number <span class="text-danger">*</span></label>
                            <input type="text" name="phone_number" value="{{ old('phone_number') }}" required>
                        </p>
                        <p>
                            <label>Date of birth</label>
                            <input type="date" name="birthdate" value="{{ old('birthdate') }}"
                        </p>
                        <p>
                            <label>Passwords <span class="text-danger">*</span></label>
                            <input type="password" name="password" required></p>
                        <p>
                            <label>Confirm Passwords<span class="text-danger">*</span></label>
                            <input type="password" name="cfpassword" required>
                        </p>
                        <div class="login_submit"><button type="submit">Register</button></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
