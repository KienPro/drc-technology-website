<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>DRC Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <link rel="stylesheet" href="{{  asset('dist/css/plugins.css') }}">
    <link rel="stylesheet" href="{{  asset('dist/css/style.css')  }}">
</head>

<body>
    <div class="error_section">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="error_form">
                        <h1>404</h1>
                        <h2>Opps! Page not Found</h2>
                        <p>Sorry but the page you are looking for does not exist, have been<br> removed, name changed or
                            is temporarily unavailable.</p>
                        <form action="{{ route('shop') }}">
                            <input placeholder="Search..." name="search" value="{{ request('search') }}" type="text">
                            <button type="submit"><i class="ion-ios-search-strong"></i></button>
                        </form>
                        <a href="{{ route('home') }}">Back to home page</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{  asset('dist/js/plugins.js')  }}"></script>
    <script src="{{  asset('dist/js/main.js')  }}"></script>
</body>

</html>