@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('home') }}">home</a></li>
                        <li>Wishlist</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wishlist_area mt-60">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="table_desc wishlist">
                    <div class="cart_page table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th class="product_remove">Remove</th>
                                    <th class="product_thumb">Image</th>
                                    <th class="product_name">Product</th>
                                    <th class="product-price">Price</th>
                                    <th class="product_total">Add To Cart</th>
                                </tr>
                            </thead>
                            <tbody id=wishlist>
                                @forelse ($data as $index => $list)
                                <tr>
                                    <td class="product_remove">
                                        <a data-toggle="modal" data-target="#modalRemove_{{ $list->product->id }}">
                                            <i class="fa fa-trash-o" aria-hidden="true "></i>
                                        </a>
                                        <div class="modal fade" id="modalRemove_{{ $list->product->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
                                              <div class="modal-content">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                <div class="modal_body">
                                                    <div class="container">
                                                        <form action="{{ route('wishlists.unwish', $list->product->id) }}" method="POST">
                                                            @csrf
                                                            @method('DELETE')
                                                            You are removing wistlist! <br>
                                                            <input type="hidden" name="customer_id" value="{{ $auth->user->id }}">
                                                            <button type="submit" class="btn btn-primary">Confirm</button>
                                                        </form>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>  
                                    </td>
                                    <td class="product_thumb">
                                        <a href="{{ asset($list->product->media->url) }}" data-lity data-lity-target="{{ asset($list->product->media->url) }}">
                                            <img src="{{ asset($list->product->media->url) }}" width="600px">
                                        </a>
                                    </td>
                                    <td class="product_name"><a href="{{ route('product-details', $list->product->id) }}">{{ $list->product->name }}</a></td>
                                    <td class="product-price">{{ formatCurrency( $list->product->current_price ) }}</td>
                                    <td class="product_total"><a @click="addToCart({{ $list->product->id }},{{ $list->product->current_price }}, {{ $auth->user->id ?? ''}})">Add To Cart</a></td>
                                </tr>
                                @empty
                                    <tr>
                                        <td colspan="99">No wishlist</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer-content')
<script src="{{ mix('dist/js/app.js') }}"></script>
<script src="{{ mix('dist/js/wishlist/wishlist.js') }}"></script>
@endsection