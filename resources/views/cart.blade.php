@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('home') }}">home</a></li>
                        <li>Shopping Cart</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="shopping_cart_area mt-60" id="cart">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="table_desc">
                    <div class="cart_page table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th class="product_remove">Delete</th>
                                    <th class="product_thumb">Image</th>
                                    <th class="product_name">Product</th>
                                    <th class="product-price">Price</th>
                                    <th class="product_quantity">Quantity</th>
                                    <th class="product_total">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(item,i) in carts">
                                    <td class="product_remove" @click="removeCart(item.id)">
                                        <a data-toggle="modal">
                                            <i class="fa fa-trash-o" aria-hidden="true "></i>
                                        </a>
                                    </td>
                                    <td class="product_thumb">
                                        <img :src="item.product.media.url" style="width: 100px">
                                    </td>
                                    <td class="product_name"><a href="#">@{{ item.product.name }}</a></td>
                                    <td class="product-price">@{{ formatCurrency( item.price ) }}</td>
                                    <td class="product_quantity"><label>Quantity</label> <input min="1" max="100" v-model="products.product[i].qty" type="number"></td>
                                    <td class="product_total">@{{ formatCurrency( item.price * item.qty ) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="cart_submit">
                        <button type="button" @click="submitOrder()">Submit Order</button>
                    </div>
                </div>
            </div>
        </div>

            {{--  <div class="coupon_area">
                <div class="row">
                    <div class="col-lg-6 col-md-6"></div>
                    <div class="col-lg-6 col-md-6">
                        <div class="coupon_code right">
                            <h3>Cart Totals</h3>
                            <div class="coupon_inner">
                                <div class="cart_subtotal">
                                    <p>Subtotal</p>
                                    <p class="cart_amount">$500.00</p>
                                </div>
                                <div class="cart_subtotal">
                                    <p>Total</p>
                                    <p class="cart_amount">$500.00</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  --}}
    </div>
</div>

@endsection
@section('footer-content')
<script src="{{ mix('dist/js/app.js') }}"></script>
<script src="{{ mix('dist/js/cart/cart.js') }}"></script>
@endsection