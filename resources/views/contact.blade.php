@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('home') }}">home</a></li>
                        <li>contact</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="contact_area mt-60">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="contact_message content">
                    <h3>contact us</h3>
                    <p>Owner: Prum Chansamedy</p>
                    
                    <ul>
                        <li><i class="fa fa-fax"></i> Address : #686A St, PHUM THAY,Krong TAKMAO,Kandal Province,Cambodia </li>
                        <li><i class="fa fa-envelope-o"> </i> Email: <a href="mailto:chan_samedy@yahoo.com">chan_samedy@yahoo.com </a>
                        </li>
                        <li><i class="fa fa-phone"></i> Tel:<a href="tel:012456896">(+855)12 456 896</a> / <a href="tel:012475758">(+855)12 475 758</a> </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="contact_message form">
                    <h3>Tell us your project</h3>
                    <form>
                        <p><input name="name" placeholder="Name *" type="text"></p>
                        <p><input name="email" placeholder="Email *" type="email"></p>
                        <p><input name="subject" placeholder="Subject *" type="text"></p>
                        <div class="contact_textarea">
                            <textarea placeholder="Message *" name="message" class="form-control2"></textarea>
                        </div>
                        <button type="submit"> Submit</button>
                        <p class="form-messege"></p>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection