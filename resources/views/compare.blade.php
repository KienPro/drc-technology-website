@extends('layouts.master')
@section('content')
    <div class="breadcrumbs_area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_content">
                        <ul>
                            <li><a href="{{  route('home') }}">home</a></li>
                            <li>compare</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content-wrap compare-page m-5" id="compare">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    {{-- <form action="#"> --}}
                        <div class="compare-table table-responsive">
                            <table class="table mb-0">
                                <tbody  v-if="productCompares.length > 0">
                                    <tr>
                                        <td class="first-column">Product</td>
                                        <td class="product-image-title" v-for="item in productCompares">
                                            <div class="image">
                                                <a :href=" '{{ url('home/product-details') }}/' + item.id"><img :src="item.image" alt="Compare Product" width="100"></a>
                                            </div>
                                            <p>
                                                <a :href=" '{{ url('home/product-details') }}/' + item.id" class="title">@{{ item.name }}</a>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="first-column">Category</td>
                                        <td v-for="item in productCompares">@{{ item.product_category }}</td>
                                    </tr>
                                    <tr>
                                        <td class="first-column">Model</td>
                                        <td v-for="item in productCompares">@{{ item.product_model }}</td>
                                    </tr>
                                    <tr>
                                        <td class="first-column">Description</td>
                                        <td class="pro-desc" v-for="item in productCompares">
                                            <p v-html="item.specification"></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="first-column">Price</td>
                                        <td class="pro-price" v-for="item in productCompares">@{{ formatCurrency(item.current_price) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="first-column">Add to cart</td>
                                        <td class="pro-addtocart" v-for="item in productCompares">
                                            <a class="add-to-cart" @click="addToSingleCart(item)"><span>ADD TO CART</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="first-column">Remove</td>
                                        <td class="pro-remove" v-for="(item, index) in productCompares">
                                            <button @click='removeCompare(item.id, index)'><i class="fa fa-trash-o"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody v-else>
                                    <tr>
                                        <td>No Product Compare!</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    {{-- </form> --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer-content')
  <script src="{{ mix('dist/js/app.js') }}"></script>
  <script src="{{ mix('dist/js/compare/compare.js') }}"></script>
@endsection