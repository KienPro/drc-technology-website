@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{ route('home') }}">home</a></li>
                        <li>My account</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="main_content_area">
    <div class="container">
        <div class="account_dashboard">
            <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-3">
                    <div class="dashboard_tab_button">
                        <ul role="tablist" class="nav flex-column dashboard-list" id="tabProfile">
                            <li><a href="#myaccount" data-toggle="tab" class="nav-link active">My Account</a>
                            <li><a href="#orders" data-toggle="tab" class="nav-link">Orders</a></li>
                            <li><a href="#changepassword" data-toggle="tab" class="nav-link">Change Password</a></li>
                            <li><a href="{{ route('logout') }}" class="nav-link">logout</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9 col-lg-9">
                    <div class="tab-content dashboard_content">
                        <div class="tab-pane fade show active" id="myaccount">
                            <h3>Account details </h3>
                            <div class="login">
                                <div class="login_form_container">
                                    <div class="account_login_form">
                                        <form action="{{ route('profile.update') }}" method="post" id="profileUpdate">
                                            @csrf
                                            <div class="input-radio">
                                                <span class="custom-radio"><input type="radio" value="male" name="gender" @if($data->gender == 'male') checked @endif> Mr.</span>
                                                <span class="custom-radio"><input type="radio" value="female" name="gender" @if($data->gender == 'female') checked @endif> Mrs.</span>
                                            </div><br>
                                            
                                            <label>Full Name</label>
                                                <input type="text" name="name" value="{{ $data->name }}">
                                            <label>Email</label>
                                                <input type="email" name="email" value="{{ $data->email }}">
                                            <label>DOB</label>
                                                <input type="date" name="birthdate" value="{{ $data->birthdate }}" placeholder="MM/DD/YYYY" >
                                            <label>Address</label>
                                                <input type="text" name="address" value="{{ $data->address }}">
                                            <div class="save_button primary_btn default_button">
                                                <button type="submit" form="profileUpdate">Save changes</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="changepassword">
                            <h3>Change Password</h3>
                            <div class="login">
                                <div class="login_form_container">
                                    <div class="account_login_form">
                                        <form action="{{ route('profile.updatepassword') }}" method="post" id="updatePassword">
                                            @csrf
                                            <label>Current Password</label>
                                                <input type="text" name="old_password">
                                            <label>New Password</label>
                                                <input type="text" name="password">
                                            <label>confirm Password</label>
                                                <input type="text" name="confirm_password">
                                            <div class="save_button primary_btn default_button">
                                                <button type="submit" form="updatePassword">Change</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="orders">
                            <h3>Orders</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Order</th>
                                            <th>Date</th>
                                            <th>Product</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>May 10, 2018</td>
                                            <td>Computer MSI</td>
                                            <td>$25.00</td>
                                            <td>2</td>
                                            <td>$50.00</td>
                                            <td><a href="cart.html" class="view">view</a></td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>May 10, 2018</td>
                                            <td>Computer Asus</td>
                                            <td>$25.00</td>
                                            <td>5</td>
                                            <td>$125.00</td>
                                            <td><a href="cart.html" class="view">view</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section("footer-content")
<script>
    // $('#tabProfile a').click(function(e) {
    //     e.preventDefault();
    //     $(this).tab('show');
    // });

    // // store the currently selected tab in the hash value
    // $("ul.nav > li > a").on("shown.bs.tab", function(e) {
    //     var id = $(e.target).attr("href").substr(1);
    //     window.location.hash = id;
    // });

    // // on load of the page: switch to the currently selected tab
    // var hash = window.location.hash;
    // $('#tabProfile a[href="' + hash + '"]').tab('show');
</script>
@endsection