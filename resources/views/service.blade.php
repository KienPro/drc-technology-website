@extends('layouts.master')
@section('content')

<div class="breadcrumbs_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_content">
                    <ul>
                        <li><a href="{{  route('home') }}">home</a></li>
                        <li>Service</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main-content-wrap compare-page m-5" id="compare">
    <div class="container">
        <div class="row py-3">
            <div class="col-md-12">
                <h3>Camera Security Solution</h3>
                <div>
                    <img src="https://i.pinimg.com/originals/81/82/69/8182696cac2fd2d3a5296f9715290ddf.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-md-12">
                <h3>Network Security Solution</h3>
                <div>
                    <img src="https://www.circlenet.co.nz/wp-content/uploads/2021/08/circlenet-it-support-services-auckland_wan-network-systems-banner.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-md-12">
                <h3>Software Solution</h3>
                <div>
                    <img src="https://www.a2zwebinfotech.com/wp-content/uploads/2021/07/software-development-banner.png" alt="">
                </div>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-md-12">
                <h3>Computer</h3>
                <div>
                    <img src="https://thecomputershop.ie/wp-content/uploads/2021/01/eletronics4.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="row py-3">
            <div class="col-md-12">
                <h3>Accessories computer</h3>
                <div>
                    <img src="https://technosmartsolutions.co.in/wp-content/uploads/2020/11/1.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection