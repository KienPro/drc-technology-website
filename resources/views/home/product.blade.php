<div class="row shop_wrapper">
	<div class="col-lg-4 col-md-4 col-12 " v-for="product in resultQuery" v-cloak>
		<article class="single_product">
			<figure>
				<div class="product_thumb">
					<a class="primary_img" :href=" '{{ url('home/product-details') }}/' + product.id"><img :src="product.media.url"></a>
					<a class="secondary_img" :href="'{{ url('home/product-details') }}/' + product.id"><img :src="product.secondary_media.url"></a>
					<div class="label_product">
						<span class="label_sale">@{{ product.is_new  == 1? "New" : "" }}</span>
					</div>
					<div class="action_links">
						<ul>
							<li class="wishlist">
								<a @click="addToWishlist(product)" title="Add to Wishlist">
									<i class="fa fa-heart-o" aria-hidden="true"></i>
								</a>
							</li>
							<li class="compare">
								<a @click="addToCompare(product.id)" title="compare">
									<span class="ion-levels"></span>
								</a>
							</li>
							<li class="quick_button">
								<a @click="quickView(product.id)" data-toggle="modal" data-target="#modal_box" title="quick view"> 
									<span class="ion-ios-search-strong"></span>
								</a>
							</li>
						</ul>
					</div>
					<div class="add_to_cart">
						<a @click="addToSingleCart(product)" title="add to cart">Add to cart</a>
					</div>
				</div>
				<div class="product_content grid_content">
					<div class="price_box">
						<span class="old_price">@{{ product.old_price == 0 ? '' : formatCurrency(product.old_price)}}</span>
						<span class="current_price">@{{ formatCurrency(product.current_price) }}</span>
					</div>
					<h3 class="product_name grid_name"><a href="#">@{{ product.name }}</a></h3>
				</div>
			</figure>
		</article>
	</div>
</div>