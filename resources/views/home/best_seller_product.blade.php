<div class="product_area mb-46">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="product_tab_btn3">
          <ul class="nav" role="tablist">
            <li><a class="active" data-toggle="tab" href="#Bestseller" role="tab" aria-controls="Bestseller" aria-selected="true"> Bestseller Products</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="tab-content">
      <div class="tab-pane fade show active" id="Bestseller" role="tabpanel">
        <div class="product_carousel product_column5 owl-carousel">
          @foreach ($product_bestsellers as $product_bestseller)
          <article class="single_product">
            <figure>
              <div class="product_thumb">
                  <a class="primary_img" href="{{ route('product-details', $product_bestseller->id) }}"><img src="{{ $product_bestseller->media->url }}" alt=""></a>
                  <a class="secondary_img" href="{{ route('product-details', $product_bestseller->id) }}"><img src="{{ $product_bestseller->secondary_media->url }}" alt=""></a>
                  <div class="label_product">
                    <span class="label_sale">{{ $product_bestseller->is_new == 1 ? "New" : "" }}</span>
                  </div>
                  {{-- <div class="action_links">
                    <ul>
                      <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i></a></li>
                      <li class="compare"><a href="#" title="compare"><span class="ion-levels"></span></a></li>
                      <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" title="quick view"> <span class="ion-ios-search-strong"></span></a></li>
                    </ul>
                  </div> --}}
                  {{-- <div class="add_to_cart">
                    <a href="cart." title="add to cart">Add to cart</a>
                  </div> --}}
              </div>
              <figcaption class="product_content">
                <div class="price_box">
                  <span class="old_price">{{ $product_bestseller->old_price == 0 ? '' : formatCurrency($product_bestseller->old_price) }}</span>
                  <span class="current_price">{{ formatCurrency($product_bestseller->current_price) }}</span>
                </div>
                <h3 class="product_name"><a href="{{ route('product-details', $product_bestseller->id) }}">{{ $product_bestseller->name }}</a></h3>
              </figcaption>
            </figure>
          </article>
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>