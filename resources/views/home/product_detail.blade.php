@extends('layouts.master')
@section('content')
<div class="breadcrumbs_area">
  <div class="container">
      <div class="row">
          <div class="col-12">
              <div class="breadcrumb_content">
                  <ul>
                      <li><a href="{{ route('home') }}">home</a></li>
                      <li>product details</li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="product_details mt-60 mb-60" >
  <div class="container">
      <div class="row">
          <div class="col-lg-6 col-md-6">
              <div class="product-details-tab">
                  <div id="img-1" class="zoomWrapper single-zoom">
                      <a href="#">
                          <img id="zoom1" src="{{ $product_details->images[0] }}"
                              data-zoom-image="{{ $product_details->images[0] }}" alt="big-1">
                      </a>
                  </div>
                  <div class="single-zoom-thumb">
                      <ul class="s-tab-zoom owl-carousel single-product-active" id="gallery_01">
                        @foreach ($product_details->images as $index => $image)
                        <li>
                            <a href="#" class="elevatezoom-gallery active" data-update=""
                            data-image="{{ $image }}"
                            data-zoom-image="{{ $image }}">
                            <img src="{{ $image }}" />
                            </a>
                        </li>
                        @endforeach
                      </ul>
                  </div>
              </div>
          </div>
          <div class="col-lg-6 col-md-6">
              <div class="product_d_right">
                <form action="{{ route('carts.addToMultiple') }}" method="post">
                    @csrf
                      <h1>{{ $product_details->name }}</h1>
                      <div class="price_box">
                          <span class="current_price">{{ formatCurrency($product_details->current_price) }}</span>
                          <span class="old_price">{{ $product_details->old_price == 0 ? '' : formatCurrency($product_details->old_price) }}</span>
                      </div>
                      <div class="product_desc">
                          <p>{{ $product_details->short_description ?? ''}}</p>
                      </div>
                      <div class="product_variant quantity">
                                <label>quantity</label>
                                <input min="1" max="100" name="qty" type="number" value="1">
                                <input type="hidden" name="product_id" value="{{ $product_details->id }}">
                                <input type="hidden" name="price" value="{{ $product_details->current_price }}">
                                <button class="button" type="submit" >add to cart</button>
                      </div>
                      <div class="product_meta">
                          <span>Category: <a href="#">{{ $product_details->category->name }}</a></span>
                      </div>
                  </form>
                  
                  <div class="priduct_social">
                      <ul>
                          <li><a class="facebook" href="https://web.facebook.com/DRC-Technology-1525528401061070" target="_blank" title="facebook"><i class="fa fa-facebook"></i>Like</a></li>
                      </ul>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<div class="product_d_info mb-60">
  <div class="container">
      <div class="row">
          <div class="col-12">
              <div class="product_d_inner">
                  <div class="product_info_button">
                      <ul class="nav" role="tablist">
                          <li><a class="active" data-toggle="tab" href="#info" role="tab" aria-controls="info" aria-selected="false">Description</a></li>
                          <li><a data-toggle="tab" href="#sheet" role="tab" aria-controls="sheet" aria-selected="false">Specification</a></li>
                      </ul>
                  </div>
                  <div class="tab-content">
                      <div class="tab-pane fade show active" id="info" role="tabpanel">
                          <div class="product_info_content">
                              <p>{{ $product_details->long_description ?? '' }}</p>
                          </div>
                      </div>
                      <div class="tab-pane fade" id="sheet" role="tabpanel">
                          <div class="product_info_content">
                            <p>{!! $product_details->specification ?? '' !!}</p>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>

<section class="product_area related_products" id="product-details">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <h2>Related Products </h2>
                </div>
            </div>
        </div>
        <div class="product_carousel product_column5 owl-carousel" >
            @foreach ($related_products as $index => $list)
            <article class="single_product">
                <figure>
                    <div class="product_thumb">
                        <a class="primary_img" href="{{ url('home/product-details', $list->id) }}"><img src="{{ $list->media->url ?? asset('dist/img/logo/drc-squre-logo.jpg') }}"></a>
                        <a class="secondary_img" href="{{ url('home/product-details', $list->id) }}"><img src="{{ $list->secondary_media->url  ?? asset('dist/img/logo/drc-squre-logo.jpg') }}"></a>
                        <div class="label_product">
                            <span class="label_sale">{{ $list->is_new == 1? "New" : "" }}</span>
                        </div>
                    </div>
                    <div class="product_content grid_content">
                        <div class="price_box">
                            <span class="old_price">{{ $list->old_price == 0 ? '' : formatCurrency($list->old_price)}}</span>
                            <span class="current_price">{{ formatCurrency($list->current_price) }}</span>
                        </div>
                        <h3 class="product_name grid_name"><a href="#">{{ $list->name }}</a></h3>
                    </div>
                </figure>
            </article>
            @endforeach
        </div>
    </div>
</section>
@endsection
@section('footer-content')
<script>
    const related_products = @json($related_products);
</script>
<script src="{{ mix('dist/js/app.js') }}"></script>
<script src="{{ mix('dist/js/product_details/product_details.js') }}"></script>
@endsection