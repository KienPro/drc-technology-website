<div class="modal fade" id="modal_box" tabindex="-1" role="dialog" aria-hidden="true" v-cloak>
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="modal_body">
        <div class="container">
          <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
              <div class="modal_tab">
                <div class="tab-content product-details-large">
                    <div class="tab-pane fade" :class="index == 0 ? 'active show' : ''" :id="'tab'+index" role="tabpanel" v-for="(item, index) in product_quickview.images">
                        <div class="modal_tab_img">
                            <a href="#"><img :src="item" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="">
                  <ul class="nav product_navactive owl-carousel list-inline" role="tablist">
                    <li v-for="(item, index) in product_quickview.images" :key='item.id' class="list-inline-item">
                      <a class="nav-link" data-toggle="tab" :href="'#tab'+index" role="tab">
                        <img :src="item" width="50px">
                      </a>
                    </li>
                  </ul>
                  {{-- <ul class="nav product_navactive owl-carousel" role="tablist">
                    <li v-for="(item, index) in product_quickview.images" :key='item.id'>
                      <a class="nav-link" data-toggle="tab" :href="'#tab'+index" role="tab">
                        <img :src="item">
                      </a>
                    </li>
                  </ul> --}}
                </div>
              </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
              <div class="modal_right">
                <div class="modal_title mb-10">
                  <h2>@{{ product_quickview.name }}</h2>
                </div>
                <div class="modal_price mb-10">
                  <span class="old_price">@{{ product_quickview.old_price == 0 ? '' : formatCurrency(product_quickview.old_price) }}</span>
                  <span class="new_price">@{{ formatCurrency(product_quickview.current_price) }}</span>
                </div>
                <div class="modal_description mb-15">
                  <p>@{{ product_quickview.short_description }}</p>
                </div>
                <div class="variants_selects">
                  <div class="modal_add_to_cart">
                    <input type="number" step="1" value="1" min="0" max="100" id="qty" v-model="multipleCarts.qty">
                    <button type="button" v-on:click="addToMultipleCart(product_quickview)">Add to cart</button>
                  </div>
                </div>
                <div class="modal_social">
                  <h2>Share this website</h2>
                  <ul>
                    <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u={{ request()->fullUrl() }}&display=popup" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="https://twitter.com/intent/tweet?url={{ request()->fullUrl() }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                    <li class="facebook"><a href="https://t.me/share/url?url={{ request()->fullUrl() }}" target="_blank"><i class="fa fa-telegram"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>