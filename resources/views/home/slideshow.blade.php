<section class="slider_section mb-70">
  <div class="slider_area owl-carousel">
    @foreach ($slideshows->list as $slideshow)
    <div class="single_slider d-flex align-items-center" data-bgimg="{{ asset($slideshow->media->url ?? 'dist/img/slider/slider1.jpg' )}}">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="slider_content">
              <h1>{{ $slideshow->title }}</h1>
              <h2>{{ $slideshow->product_name }}</h2>
              <p>{{ $slideshow->tag_line }}</p>
              @if($slideshow->url && $slideshow->button_name)
                <a class="button" href="{{ $slideshow->url }}">{{ $slideshow->button_name }}</a>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</section>