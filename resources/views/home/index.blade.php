@extends('layouts.master')

@section('content')
  @include('home.slideshow')

  @include('home.best_seller_product')

  <div class="shop_area shop_reverse mt-60 mb-60" id="home" v-cloak>
    <div class="container">
      <div class="row">
        <!-- Filter -->
        <div class="col-lg-3 col-md-12">
          <aside class="sidebar_widget">
            <div class="widget_inner">
              <!-- Product Filter -->
              <div class="widget_list widget_categories" >
                <h2>Product Filter</h2>
                <ul>
                  <li class="widget_sub_categories"  v-for="product_category in product_categories">
                    <a class="parent" :data-id="`cat-${product_category.id}`">
                      @{{ product_category.name }}
                    </a>
                    <ul class="widget_dropdown_categories" :id="`cat-${product_category.id}`" style="display: none">
                        <li><a v-on:click="product_model_id = product_model.id; product_model_selected = product_model.id" v-for="product_model in product_category.product_models" :class="{active:product_model.id == product_model_selected}">@{{ product_model.name }}</a></li>
                    </ul>
                  </li>
                  <li><a v-on:click="product_model_selected=undefined; product_model_id=null" :class="{active : undefined == product_model_selected}">Uncategories</a></li>

                </ul>
              </div>
              <!--/ Product Filter -->
              <!-- Price Filter -->
              <div class="widget_list widget_filter">
                <h2>Filter by price</h2>
                <form>
                  <div id="slider-range"></div>
                  <button type="button" v-on:click="filterByPriceRange()">Filter</button>
                  <input type="text" name="text" id="amount" readonly/>
                </form>
              </div>
              <!--/ Price Filter -->
              <!-- Product tags Filter -->
              <div class="widget_list tags_widget">
                <h2>Product tags</h2>
                <div class="tag_cloud">
                    <a v-on:click="product_tag_selected=undefined; product_tag_name=''" :class="{selected: 'undefined' === product_tag_selected || undefined === product_tag_selected}">All</a>
                    <a :class="{selected:product_tag.name == product_tag_selected}" v-for="product_tag in product_tags"  v-on:click="product_tag_name = product_tag.name; product_tag_selected = product_tag.name">@{{ product_tag.name }}</a>
                </div>
              </div>
              <!--/ Product tags Filter -->
            </div>
          </aside>
        </div>
        <!--/ Filter -->

        <div class="col-lg-9 col-md-12">
          <div class="shop_toolbar_wrapper">
            <div class="shop_toolbar_btn">
              <button data-role="grid_3" type="button" class="active btn-grid-3" data-toggle="tooltip" title="3"></button>
              <button data-role="grid_4" type="button" class=" btn-grid-4" data-toggle="tooltip" title="4"></button>
            </div>
            <!-- Sort -->
            {{-- <div class="page_amount niceselect_option" style="width: 120px" >
              <select class="sortby-select2" v-select2  style="width: 100%" v-model="sort">
                <option value="name">Sort by name</option>
                <option value="current_price">Sort by price</option>
              </select>
            </div> --}}
            <!--/ Sort -->
          </div>

         @include('home.product')

          <!-- Pagination -->
          <div class="shop_toolbar t_bottom" v-if="total > products.length" >
            <div class="pagination">
              <button class="btn btn-primary" style="width: 100%" @click="init()">Load More...</button>
            </div>
          </div>
          <!-- Pagination -->
        </div>
      </div>
    </div>
    <!-- Quick view modal -->
    @include('home.modal_quick_view')
    <!--/ Quick view modal -->
  </div>

@endsection
@section('footer-content')
  <script>
    const product_categories = <?php echo json_encode($product_categories); ?>;
    const product_tags = <?php echo json_encode($product_tags); ?>;
    const search = <?php echo ("'".request('search')."'" ?? "''") ?> ;
    const is_new = <?php echo ("'".request('is_new')."'" ?? "''") ?> ;
    const product_model_id = <?php echo ("'".request('product_model_id')."'" ?? "''") ?> ;
  </script>
  <script src="{{ mix('dist/js/app.js') }}"></script>
  <script src="{{ mix('dist/js/home/home.js') }}"></script>
  <script>
    $('.sortby-select2').select2({
      minimumResultsForSearch: -1
    });
</script>

@endsection

