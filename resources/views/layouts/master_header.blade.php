<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>DRC Technology</title>

    <meta name="author" content="DRC Technology">
    <meta name="title" content="DRC Technology - We Provide good and Services!">
    <meta name="keywords" content="Ecommerce, Computer Shop, CCTV, Camera Security">
    <meta name="description" content="DRCTechnology Computer Shop is the shop that offers variety of technology products brand such as Computer, Accessories, CCTV, and Hardware.">
    <meta itemprop="image" content="{{ asset('dist/img/logo/drc-transparent-banner.png') }}">

    <!-- facebook meta-->
    <meta property="og:type" content="website">
    <meta property="og:url" content="/">
    <meta property="og:site_name" content="DRC Technology">
    <meta property="og:title" content="DRC Technology - We Provide good and Services!">
    <meta property="og:description" content="DRCTechnology Computer Shop is the shop that offers variety of technology products brand such as Computer, Accessories, CCTV, and Hardware.">
    <meta property="og:image" content="{{ asset('dist/img/logo/drc-transparent-banner.png') }}">

    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="/">
    <meta property="twitter:title" content="DRC Technology - We Provide good and Services!">
    <meta property="twitter:description" content="DRCTechnology Computer Shop is the shop that offers variety of technology products brand such as Computer, Accessories, CCTV, and Hardware.">
    <meta property="twitter:image" content="{{ asset('dist/img/logo/drc-transparent-banner.png') }}">


    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- IOS web app -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <link rel="apple-touch-icon" href="{{ asset('dist/img/logo/drc-squre-logo.jpg') }}">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('dist/img/logo/drc-squre-logo.jpg') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/plugins.css') }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset('dist/plugins/select2/css/select2.min.css') }}">
    <!-- Jquery toast -->
    <link rel="stylesheet" href="{{ asset('dist/plugins/loading/jquery.loading.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/plugins/toast/jquery.toast.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/plugins/confirm/jquery-confirm.min.css') }}">

    <!-- lity -->
    <link rel="stylesheet" href="{{ asset('dist/plugins/lity-2.4.1/dist/lity.css') }}">
    <!-- Custom css -->
    <link rel="stylesheet" href="{{ asset('dist/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}">
      <style>
          .modal_add_to_cart input {
              width: 95px;
              border: 1px solid #ebebeb;
              background: none;
              padding: 0 10px;
              height: 45px;
          }

          .modal_add_to_cart button {
              background: none;
              border: 1px solid #242424;
              margin-left: 10px;
              font-size: 12px;
              font-weight: 700;
              height: 45px;
              width: 230px;
              line-height: 18px;
              padding: 10px 15px;
              text-transform: uppercase;
              background: #0063d1;
              color: #ffffff;
              -webkit-transition: 0.3s;
              transition: 0.3s;
              cursor: pointer;
          }

          .modal_add_to_cart button:hover {
              background: #242424;
              color: #ffffff;
              border-color: #0063d1;
          }
      </style>
      @yield('styles')
  </head>
