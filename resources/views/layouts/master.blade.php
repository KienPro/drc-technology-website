
@php(getGeneralData($auth))
@include('layouts.master_header')
<body>
   <!-- Preloader -->
	<div class="preloader">
		<div class="preloader-inner">
			<div class="preloader-icon">
				<span></span>
				<span></span>
			</div>
		</div>
	</div>
	<!-- End Preloader -->
   @include('layouts.header')
   @yield('content')   
   <footer class="footer_widgets">
      @include('layouts.footer')
   </footer>    
</body>
@include('layouts.master_footer')
@include('layouts.alert')
