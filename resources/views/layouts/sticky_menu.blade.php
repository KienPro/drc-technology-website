<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-3">
            <div class="logo">
                <a href="{{ route('home') }}"><img src="{{ asset('dist/img/logo/drc-transparent-logo.png') }}" style="width: 90px; padding: 10px"></a>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="sticky_header_right menu_position">
                <div class="main_menu">
                    <nav>
                        <ul>
                            <li><a class="{{ request()->is(['/', '*home*', '*product_details*', '*shop*']) ? 'active' : '' }}" href="{{ route('home') }}">Home</a></li>
                            <li><a class="{{ request()->is('*compare') ? 'active' : '' }}" href="{{ route('compare') }}">Compare</a></li>
                            <li><a class="{{ request()->is('*service') ? 'active' : '' }}" href="{{ route('service') }}">Service</a></li>
                            {{-- <li><a class="{{ request()->is('*about') ? 'active' : '' }}" href="{{ route('about') }}">About Us</a></li> --}}
                            <li><a class="{{ request()->is('*contact') ? 'active' : '' }}"href="{{ route('contact') }}"> Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="middel_right_info">
                      <div class="header_wishlist">
                        <form>
                        <div class="">
                            <input type="hidden" name="is_new" value="2">
                            <button type="submit"><i class="fa fa-archive" aria-hidden="true" title="New Product"></i></button>
                            @if($count > 0)
                            <span class="test">{{ $count }}</span>
                            @else
                            <span class="test">0</span>
                            @endif
                        </div>
                        </form>
                    </div>
                    <div class="header_wishlist">
                        <a href="{{ route('wishlists') }}">
                            @if($auth)
                                <i class="fa fa-heart-o" aria-hidden="true"></i>
                            @else
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            @endif
                        </a>
                        <span class="wishlist_quantity">{{ $count_wishlist ?? 0 }}</span>
                    </div>
                    <div class="mini_cart_wrapper">
                        <a href="javascript:void(0)"><i class="fa fa-shopping-bag"
                                                        aria-hidden="true"></i>{{ $total ?? 0 }} <i class="fa fa-angle-down"></i></a>
                                                        <span class="cart_quantity">{{ $count_cart ?? 0 }}</span>

                        <!--mini cart-->
                        <div class="mini_cart" style="overflow: auto">
                            @if($auth)
                                @foreach ($cart_list as $index => $list)
                                <div class="cart_item">
                                    <div class="cart_img">
                                        <a href="#"><img src="{{ asset($list->product->media->url) }}" alt=""></a>
                                    </div>
                                    <div class="cart_info">
                                        <a href="#">{{ $list->product->name }}</a>
                                        <p>Qty: {{ $list->qty }} X <span> {{ formatCurrency($list->price) }} </span></p>
                                    </div>
                                    <div class="cart_remove">
                                        <form action="{{ route('carts.delete', $list->id) }}" method="post">
                                            @csrf
                                            <button type="submit"><i class="ion-android-close"></i></button>
                                        </form>
                                    </div>
                                </div>
                                @endforeach
                                <div class="mini_cart_footer">
                                    <div class="cart_button">
                                        <a href="{{ route('view-carts') }}">View cart</a>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>