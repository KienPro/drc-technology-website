<div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="widgets_container contact_us">
                        <div class="footer_logo">
                            <a href="#"><img src="{{ asset('dist/img/logo/drc-transparent-logo.png') }}" alt=""></a>
                        </div>
                        <div class="footer_contact">
                            <h3>Prum Chansamedy</h3>
                            <p><span>Address</span> #686A St, PHUM THAY,Krong TAKMAO,Kandal Province,Cambodia</p>
                            <p><span>Mobile: </span><a href="tel:012456896">012 456 896</a> – <a href="tel:012475758">012 475 758</a></p>
                            <p><span>Mail: </span><a href="mailto:chan_samedy@yahoo.com">chan_samedy@yahoo.com</a></p>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widgets_container widget_menu">
                        <h3>Information</h3>
                        <div class="footer_menu">
                            <ul>
                                <li><a href="{{ route('contact') }}">Contact US</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widgets_container widget_menu">
                        <h3>My Account</h3>
                        <div class="footer_menu">
                            <ul>
                                <li><a href="{{ route('profile') }}">My Account</a></li>
                                <li><a href="{{ route('wishlists') }}">Wish List</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="widgets_container newsletter">
                        <h3>Follow Us</h3>
                        <div class="footer_social_link">
                            <ul>
                                <li><a class="facebook" href="https://www.facebook.com/DRC-Technology-1525528401061070" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                {{-- <li><a class="twitter" href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li> --}}
                                {{-- <li><a class="instagram" href="#" title="instagram"><i class="fa fa-instagram"></i></a></li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-6">
                    <div class="copyright_area">
                        <p class="copyright-text"> &copy; 2021 <a href="#">DRC</a>. Creators
                            <a href="#" target="_blank">Captain Team</a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="footer_payment text-right">
                        <a href="#"><img src="{{ asset('dist/img/icon/payment.png') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>