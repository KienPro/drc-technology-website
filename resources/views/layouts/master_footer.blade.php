    <script src="{{ asset('dist/js/script/mordernizr.js') }}"></script>
    <script src="{{ asset('dist/js/script/jQuery-v1.12.4.js') }}"></script>
    <script src="{{ asset('dist/js/script/popper.js') }}"></script>
    <script src="{{ asset('dist/js/script/Bootstrap-v4.1.1.js') }}"></script>
    <script src="{{ asset('dist/js/script/jQuery-meanMenu.js') }}"></script>
    <script src="{{ asset('dist/js/script/slick-slider.js') }}"></script>
    <script src="{{ asset('dist/js/script/magnific-popup.js') }}"></script>
    <script src="{{ asset('dist/js/script/wow.js') }}"></script>
    <script src="{{ asset('dist/js/script/jquery.counterup.js') }}"></script>
    <script src="{{ asset('dist/js/script/jQuery-Waypoints.js') }}"></script>
    <script src="{{ asset('dist/js/script/jquery-nice-select.js') }}"></script>
    <script src="{{ asset('dist/js/script/jquery-collapse.js') }}"></script>
    <script src="{{ asset('dist/js/script/scrollup.js') }}"></script>
    <script src="{{ asset('dist/js/script/Owl-Carousel-v2.2.1.js') }}"></script>
    <script src="{{ asset('dist/js/script/thefinalcountdown.js') }}"></script>
    <script src="{{ asset('dist/js/script/jQuery-UI.js') }}"></script>
    <script src="{{ asset('dist/js/script/jQuery-elevateZoom.js') }}"></script>
    <script src="{{ asset('dist/js/script/imageLoaded.js') }}"></script>
    <script src="{{ asset('dist/js/script/Isotope.js') }}"></script>
    <script src="{{ asset('dist/js/script/jQuery-Cookie.js') }}"></script>
    <script src="{{ asset('dist/js/script/bPopup.js') }}"></script>
    <script src="{{ asset('dist/js/script/slinky-menu.js') }}"></script>
    <script src="{{ asset('dist/js/script/Mailchimp.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('dist/plugins/select2/js/select2.min.js') }}"></script>
    <!-- Jquery toast -->
    <script src="{{ asset('dist/plugins/loading/jquery.loading.js') }}"></script>
    <script src="{{ asset('dist/plugins/toast/jquery.toast.min.js') }}"></script>
    <script src="{{ asset('dist/plugins/confirm/jquery-confirm.min.js') }}"></script>

    <script src="{{ asset('dist/plugins/lity-2.4.1/dist/lity.js') }}"></script>

    <script src="{{ asset('dist/js/main.js') }}"></script>

    <script src="{{ asset('js/helper.js') }}"></script>

    @yield('footer-content')

    <script>
        $(".widget_sub_categories").on("click", '.parent', function(e) {
            const id = $(this).attr('data-id')
            $(this).toggleClass('active');
            // $('.widget_dropdown_categories').slideDown('medium');
            $(`.widget_dropdown_categories#${id}`).slideToggle('medium')
        });
    </script>

    <script src="{{ asset('dist/js/product-navactive.js') }}"></script>
    <script src="{{ asset('dist/js/own-carousel.js') }}"></script>
    <script src="{{ asset('dist/js/slider-activation.js') }}"></script>
    <script src="{{ asset('dist/js/shop-grid-activation.js') }}"></script>
    <script src="{{ asset('dist/js/portfolio-grid.js') }}"></script>
    <script src="{{ asset('dist/js/slider-range.js') }}"></script>
    <script src="{{ asset('dist/js/nice-select.js') }}"></script>
</html>
