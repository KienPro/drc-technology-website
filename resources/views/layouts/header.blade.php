<div class="off_canvars_overlay"></div>
<div class="Offcanvas_menu">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="canvas_open">
          <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
        </div>
        <div class="Offcanvas_menu_wrapper">
          <div class="canvas_close">
            <a href="javascript:void(0)"><i class="ion-android-close"></i></a>
          </div>
          <div class="support_info">
            <p>Contact Us: <a href="tel:012456896">(+855)12 456 896</a> / <a href="tel:012475758">(+855)12 475 758</a></p>
          </div>
          <div class="top_right text-right">
            <ul>
              <li><a href="{{ route('profile') }}"> My Account </a></li>
            </ul>
          </div>
          <div class="search_container">
            <form>
              <div class="search_box">
                <input placeholder="Search product..." type="text" name="search" value="{{ request('search') }}">
                <button type="submit">Search</button>
              </div>
            </form>
          </div>
          <div class="middel_right_info">
            <div class="header_wishlist">
             <form>
              <div class="">
                <input type="hidden" name="is_new" value="2">
                <button type="submit"><i class="fa fa-archive" aria-hidden="true" title="New Product"></i></button>
                @if($count > 0)
                  <span class="test">{{ $count }}</span>
                @else
                  <span class="test">0</span>
                @endif
              </div>
            </form>
          </div>
            {{-- <div class="header_wishlist">
              <a href="{{ route('home') . '?is_new=1' }}">
                <i class="fa fa-archive" aria-hidden="true"></i>
              </a>
              <span class="wishlist_quantity">{{ $count_wishlist ?? 0 }}</span>
            </div> --}}
            <div class="header_wishlist">
                <a href="{{ route('wishlists') }}">
                    @if($auth)
                        <i class="fa fa-heart-o" aria-hidden="true"></i>
                    @else
                        <i class="fa fa-heart" aria-hidden="true"></i>
                    @endif
                </a>
                <span class="wishlist_quantity">{{ $count_wishlist ?? 0 }}</span>
            </div>          
            <div class="mini_cart_wrapper">
                <a href="javascript:void(0)">
                  <i class="fa fa-shopping-bag" aria-hidden="true"></i>{{ $total ?? 0 }} <i class="fa fa-angle-down"></i>
                </a>
                <span class="cart_quantity">{{ $count_cart ?? 0 }}</span>

                <!--mini cart-->
                <div class="mini_cart" style="overflow: auto">
                    @if($auth)
                        @foreach ($cart_list as $index => $list)
                        <div class="cart_item">
                            <div class="cart_img">
                                <a href="#"><img src="{{ asset($list->product->media->url) }}" alt=""></a>
                            </div>
                            <div class="cart_info">
                                <a href="#">{{ $list->product->name }}</a>
                                <p>Qty: {{ $list->qty }} X <span> {{ formatCurrency($list->price) }} </span></p>
                            </div>
                            <div class="cart_remove">
                                <form action="{{ route('carts.delete', $list->id) }}" method="post">
                                    @csrf
                                    <button type="submit"><i class="ion-android-close"></i></button>
                                </form>
                            </div>
                        </div>
                        @endforeach
                        <div class="mini_cart_footer">
                            <div class="cart_button">
                                <a href="{{ route('view-carts') }}">View cart</a>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
          </div>

            <div id="menu" class="text-left ">
                <ul class="offcanvas_main_menu">
                    <li><a class="{{ request()->is(['*/', '*shop*', '*product_details*', '*home*']) ? 'active' : '' }}" href="{{ route('home') }}">Home</a></li>
                    <li><a class="{{ request()->is('*compare') ? 'active' : '' }}" href="{{ route('compare') }}">Compare</a></li>
                    <li><a class="{{ request()->is('*service') ? 'active' : '' }}" href="{{ route('service') }}">Service</a></li>
                    {{-- <li><a class="{{ request()->is('*about') ? 'active' : '' }}" href="{{ route('about') }}">About Us</a></li> --}}
                    <li><a class="{{ request()->is('*contact') ? 'active' : '' }}"href="{{ route('contact') }}"> Contact Us</a></li>
                </ul>
            </div>

            <div class="Offcanvas_footer">
                <span><a href="#"><i class="fa fa-envelope-o"></i> info@drc.com</a></span>
                <ul>
                    <li class="facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li class="linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

<header>
  <div class="main_header">
    @include('layouts.main_menu')
  </div>
</header>

<div class="sticky_header_area sticky-header">
  @include('layouts.sticky_menu')
</div>
