<div class="header_top">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-6">
                <div class="support_info">
                    <p>Contact Us: <a href="tel:012456896">012 456 896</a> / <a href="tel:012475758">012 475 758</a></p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="top_right text-right">
                    <ul>
                        @if($auth)
                            <li><a href="{{ route('profile') }}" class="{{ request()->is('*profile') ? 'active' : '' }}"> Hi, {{ $auth->user->name ?? 'Unknown' }} </a></li>
                        @else
                            <li><a href="{{ route('auth.login.index') }}" class="{{ request()->is('*auth/login') ? 'active' : '' }}"> Login </a> or <a href="{{ route('auth.register.index') }}" class="{{ request()->is('*auth/register') ? 'active' : '' }}"> Register </a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="header_middle">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-6">
                <div class="logo">
                    <a href="#"><img src="{{ asset('dist/img/logo/drc-transparent-logo.png') }}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-9 col-md-6">
                <div class="middel_right">
                    <div class="search_container">
                        <form action="{{ route('shop') ."?search=". request('search')}}">
                            <div class="search_box">
                                <input name="search" type="text" placeholder="Search product..." value="{{ request('search') }}">
                                <button type="submit">Search</button>
                            </div>
                        </form>
                    </div>
                    <div class="middel_right_info">
                          <div class="header_wishlist">
                            <form>
                            <div class="">
                                <input type="hidden" name="is_new" value="2">
                                <button type="submit"><i class="fa fa-archive" aria-hidden="true" title="New Product"></i></button>
                                @if($count > 0)
                                <span class="test">{{ $count }}</span>
                                @else
                                <span class="test">0</span>
                                @endif
                            </div>
                            </form>
                        </div>
                        <div class="header_wishlist">
                            <a href="{{ route('wishlists') }}">
                                @if($auth)
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                @else
                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                @endif
                            </a>
                            <span class="wishlist_quantity">{{ $count_wishlist ?? 0 }}</span>
                        </div>
                        <div class="mini_cart_wrapper">
                            <a href="javascript:void(0)">
                              <i class="fa fa-shopping-bag" aria-hidden="true"></i>{{ $total ?? 0 }} <i class="fa fa-angle-down"></i>
                            </a>
                            <span class="cart_quantity">{{ $count_cart ?? 0 }}</span>
            
                            <!--mini cart-->
                            <div class="mini_cart" style="overflow: auto">
                                @if($auth)
                                    @foreach ($cart_list as $index => $list)
                                    <div class="cart_item">
                                        <div class="cart_img">
                                            <a href="#"><img src="{{ asset($list->product->media->url) }}" alt=""></a>
                                        </div>
                                        <div class="cart_info">
                                            <a href="#">{{ $list->product->name }}</a>
                                            <p>Qty: {{ $list->qty }} X <span> {{ formatCurrency($list->price) }} </span></p>
                                        </div>
                                        <div class="cart_remove">
                                            <form action="{{ route('carts.delete', $list->id) }}" method="post">
                                                @csrf
                                                <button type="submit"><i class="ion-android-close"></i></button>
                                            </form>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="mini_cart_footer">
                                        <div class="cart_button">
                                            <a href="{{ route('view-carts') }}">View cart</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main_menu_area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-12">
                <div class="categories_menu">
                    <div class="categories_title">
                        <h2 class="categori_toggle">ALL CATEGORIES</h2>
                    </div>
                    <div class="categories_menu_toggle">
                        <ul>      
                            @foreach ($product_categories as $index => $product_category)
                            <li class="menu_item_children"><a href="#">{{ $product_category->name }}
                                <i class="fa fa-angle-right"></i></a>
                                <ul class="categories_mega_menu">
                                    
                                    <li class="menu_item_children"><a href="#">{{ $product_category->name }}</a>
                                        <ul class="categorie_sub_menu">
                                            @foreach ($product_category->product_models as $index => $product_model)
                                                <li><a href="{{ route('shop') ."?product_model_id=". $product_model->id }}">{{ $product_model->name }}</a></li>
                                            @endforeach  
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="main_menu menu_position">
                    <nav>
                        <ul>
                            <li><a class="{{ request()->is(['*/', '*shop*', '*product_details*', '*home*']) ? 'active' : '' }}" href="{{ route('home') }}">Home</a></li>
                            <li><a class="{{ request()->is('*compare') ? 'active' : '' }}" href="{{ route('compare') }}">Compare</a></li>
                            <li><a class="{{ request()->is('*service') ? 'active' : '' }}" href="{{ route('service') }}">Service</a></li>
                            {{-- <li><a class="{{ request()->is('*about') ? 'active' : '' }}" href="{{ route('about') }}">About Us</a></li> --}}
                            <li><a class="{{ request()->is('*contact') ? 'active' : '' }}"href="{{ route('contact') }}"> Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>