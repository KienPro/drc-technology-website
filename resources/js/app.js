require('./bootstrap');
window.Vue = require('vue');
var _ = require('lodash');

Vue.directive('select2', {
    inserted(el) {
        $(el).on('select2:select', () => {
            const event = new Event('change', {bubbles: true, cancelable: true});
            el.dispatchEvent(event);
        });
    },
});