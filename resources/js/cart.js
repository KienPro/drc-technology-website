const { each } = require("lodash");

new Vue({
    el: '#cart',
    data: {
        carts: [],
        products: {
            product: []
        }
    },
    mounted() {
        this.init();
    },
    methods: {
        init(){
            axios.get(`carts/item`)
            .then(response => {
                if (response.data.success) {
                    this.carts.push(...response.data.data.list)
                    response.data.data.list.forEach(element => {
                        this.products.product.push({product_id: element.product.id, qty: element.qty});
                    });

                } else {
                    console.log('response not success');
                }
            });
        }, 

        submitOrder(){
            showLoading();
            axios.post(`/order`, 
                this.products
            ).then(response => {
                if (response.data.success) {
                    hideLoading();
                    window.location.href = "home"
                } else {
                    console.log('response not success');
                }
            });
        },
        removeCart(id){
            console.log(id);
            showLoading();
            axios.post(`carts/remove/${id}`)
            .then(response => {
                if (response.data.success) {
                    hideLoading();
                    showToastSuccess('Cart removed successfully!');
                    window.location.href = "carts"
                } else {
                    console.log('response not success');
                }
            });
        },

        formatCurrency(money)
        {
            var formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 2
            });
            return formatter.format(money);
        },
    }

});