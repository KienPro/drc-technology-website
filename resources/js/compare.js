new Vue({
    el: '#compare',
    data: {
        productCompares: [],
        singleCarts:{
            product_id: null,
            qty: 1,
            price: null,
            amount: null
        }
    },
    mounted() {
        this.productCompares = JSON.parse(localStorage.getItem('compare'));
    },
    methods:{
        removeCompare(id){ 
            this.productCompares.splice(this.productCompares.findIndex(v => v.id === id), 1);
            localStorage.setItem('compare', JSON.stringify(this.productCompares));
        },
       
        
        addToSingleCart(product){
            this.singleCarts.product_id = product.id
            this.singleCarts.price = product.current_price
            showLoading();
            axios.post(`/carts/add/single_cart`,
                    this.singleCarts
            ).then(response => {
                if (response.data.success) {
                    hideLoading()
                    window.location.href = "/carts";
                    showToastSuccess('Product added to <a href="carts">Cart</a>.');
                } else {
                    window.location.href = "/carts"
                    // showToastError('Product already exist in <a href="wishlists">Wishlist</a>.');
                }
                // window.location.href = "carts"  
            });
        },    
        formatCurrency(money)
        {
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2
            });
            return formatter.format(money);
        },
    }

});