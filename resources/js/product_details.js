const { defaultsDeep } = require("lodash");

Home = new Vue({
    el: '#product-details',
    data: {
        related_products:related_products,
        singleCarts:{
            product_id: null,
            qty: 1,
            price: null,
            amount: null
        },

        compareItems: JSON.parse(localStorage.getItem('compare')) || [],

        wishlists:{
            product_id: null,
            price: null,
        },
    },
    methods: { 
         
        addToCompare(id){
            showLoading();
            axios.get(`/home/product/compare/${id}`
                ).then(response => {
                if (response.data.success) {
                    hideLoading()
                    if(JSON.parse(localStorage.compare).length > 1){
                        showToastWarning('You can compare item only 2 in <a href="/compare">Compare</a>.');
                        return;
                    }
                    showToastSuccess('Product added to <a href="/compare">Compare</a>.');
                    this.compareItems.push(response.data.data);
                    localStorage.setItem('compare', JSON.stringify(this.compareItems));
                } else {
                    console.log('response not success');
                }
            })
            
        },

        addToWishlist(product){
            this.wishlists.product_id = product.id
            this.wishlists.price = product.current_price
            showLoading();
            axios.post(`/wishlists/wish`,
                    this.wishlists
            ).then(response => {
                if (response.data.success) {
                    hideLoading()
                    window.location.href = "/wishlists";
                    showToastSuccess('Product added to <a href="wishlists">Wishlists</a>.');
                } else {
                    window.location.href = "/wishlists"
                    // hideLoading();
                    // showToastError('Product already exist in <a href="wishlists">Wishlists</a>.');
                }
            });
        },    
        
        addToSingleCart(product){
            this.singleCarts.product_id = product.id
            this.singleCarts.price = product.current_price
            showLoading();
            axios.post(`/carts/add/single_cart`,
                    this.singleCarts
            ).then(response => {
                if (response.data.success) {
                    hideLoading()
                    window.location.href = "/carts";
                    showToastSuccess('Product added to <a href="carts">Cart</a>.');
                } else {
                    window.location.href = "/carts"
                    // showToastError('Product already exist in <a href="wishlists">Wishlist</a>.');
                }
                // window.location.href = "carts"  
            });
        },    
        formatCurrency(money)
        {
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2
            });
            return formatter.format(money);
        },
    }
});