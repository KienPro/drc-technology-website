const { defaultsDeep } = require("lodash");

new Vue({
    el: '#wishlist',
    data: {
        carts:{
            product_id: null,
            customer_id: null, 
            qty: 1,
            price: null,
            amount: null
        }
    },
    methods: {  
        addToCart(product,current_price ,customer_id){
            this.carts.product_id = product
            this.carts.customer_id = customer_id
            this.carts.price = current_price
            this.carts.amount = current_price
            axios.post(`carts/add/single_cart`,
                    this.carts
            ).then(response => {
                if (response.data.success) {
                    showToastSuccess('Product added to <a href="carts">Cart</a>.');
                } else {
                    showToastError('Product already exist in <a href="wishlists">Wishlist</a>.');
                }
                window.location.href = "wishlists"
            });
        }, 
    }    
});