const { defaultsDeep } = require("lodash");

Home = new Vue({
    el: '#home',
    data: {
        cart:[],
        product_categories: product_categories,
        product_tags: product_tags,
        product_quickview: [],

        products: [],
        total: 0,
        search: search,
        is_new: is_new,
        searchQuery: null,
        
        product_model_selected: undefined,
        product_tag_selected: undefined,
        sort: 'name',

        product_model_id: product_model_id ?? null,
        price_range:null,
        minval: 0,
        maxval: 3000,
        product_tag_name: '',

        compareItems: JSON.parse(localStorage.getItem('compare')) || [],

        wishlists:{
            product_id: null,
            price: null,
        },

        singleCarts:{
            product_id: null,
            qty: 1,
            price: null,
            amount: null
        },
        multipleCarts:{
            product_id: null,
            qty: 1,
            price: null,
            amount: null
        }
    },
    mounted() {
        this.init();
    },
    methods: {  
        init(){
            showLoading();
            axios.get(`home/product?offset=${this.products.length}&search=${this.search}&is_new=${this.is_new}&product_model_id=${this.product_model_id}`
                ).then(response => {
                if (response.data.success) {
                    hideLoading()
                    this.products.push(...response.data.data.list);
                    this.total = response.data.data.total;
                } else {
                    console.log('response not success');
                }
            })
        },    
        
        addToCompare(id){
            console.log(id);
            showLoading();
            axios.get(`home/product/compare/${id}`
                ).then(response => {
                if (response.data.success) {
                    hideLoading()
                    if (localStorage.getItem("compare") !== null) {
                        if(JSON.parse(localStorage.compare).length >=2){
                            console.log(JSON.parse(localStorage.compare).length);
                            showToastWarning('You can compare item only 2 in <a href="compare">Compare</a>.');
                            return;
                        }
                    }
                    showToastSuccess('Product added to <a href="compare">Compare</a>.');
                    this.compareItems.push(response.data.data);
                    localStorage.setItem('compare', JSON.stringify(this.compareItems));
                } else {
                    console.log('response not success');
                }
            })
            
        },

        addToWishlist(product){
            this.wishlists.product_id = product.id
            this.wishlists.price = product.current_price
            showLoading();
            axios.post(`wishlists/wish`,
                    this.wishlists
            ).then(response => {
                if (response.data.success) {
                    hideLoading()
                    window.location.href = "wishlists";
                    showToastSuccess('Product added to <a href="wishlists">Wishlists</a>.');
                } else {
                    window.location.href = "wishlists"
                    // hideLoading();
                    // showToastError('Product already exist in <a href="wishlists">Wishlists</a>.');
                }
            });
        },    
        
        addToSingleCart(product){
            this.singleCarts.product_id = product.id
            this.singleCarts.price = product.current_price
            showLoading();
            axios.post(`carts/add/single_cart`,
                    this.singleCarts
            ).then(response => {
                if (response.data.success) {
                    hideLoading()
                    window.location.href = "carts";
                    // showToastSuccess('Product added to <a href="carts">Cart</a>.');
                } else {
                    window.location.href = "carts"
                    // showToastError('Product already exist in <a href="wishlists">Wishlist</a>.');
                }
                // window.location.href = "carts"  
            });
        },    
        addToMultipleCart(product){
            this.multipleCarts.product_id = product.id
            this.multipleCarts.price = product.current_price
            showLoading();
            axios.post(`carts/add/multiple_cart`,
                    this.multipleCarts
            ).then(response => {
                if (response.data.success) {
                    hideLoading()
                    window.location.href = "carts";
                    // showToastSuccess('Product added to <a href="carts">Cart</a>.');
                } else {
                    window.location.href = "carts"
                    // showToastError('Product already exist in <a href="wishlists">Wishlist</a>.');
                }
                // window.location.href = "carts"  
            });
        },    

        quickView(id){
            axios.get(`home/quickview/${id}`
                ).then(response => {
                if (response.data.success) {
                    this.product_quickview = response.data.data;
                    localStorage.setItem('productQuickview', JSON.stringify(response.data.data));
                } else {
                    console.log('response not success');
                }
            })
        },

        formatCurrency(money)
        {
            var formatter = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
                minimumFractionDigits: 2
            });
            return formatter.format(money);
        },
        
        filterByPriceRange() {
            var minval = $("#slider-range" ).slider( "values", 0 ); 
            var maxval = $("#slider-range" ).slider( "values", 1 ); 

            this.minval = minval;
            this.maxval = maxval;
        },
        updateURL() {
            let qp = new URLSearchParams();
            if(this.product_model_id !== '') qp.set('product_model_id', this.product_model_id);
            if(this.product_tag_name !== '') qp.set('product_tag_name', this.product_tag_name);
            if(this.minval !== 0) qp.set('price_range', this.minval +"-"+ this.maxval);
            history.replaceState(null, null, "?"+qp.toString());
        }
    },
    computed: {
        resultQuery()
        {
            this.updateURL();
            var vm = this, lists = vm.products
            return _.filter(lists, function(query){
                var product_model = vm.product_model_id ? (query.product_model_id == vm.product_model_id) : true,
                    product_tag = vm.product_tag_name ? query.string_tags.includes(vm.product_tag_name) : true,
                    current_price = query.current_price >= vm.minval && query.current_price <= vm.maxval;
                return product_model && product_tag && current_price;
            });
        },
    }
});