<?php
    function getGeneralData($auth)
    {
        $controller = new \App\Http\Controllers\Controller();
        $controller->setAuth($auth);
        if ($auth) {
            $count_wishlist = $controller->api_get('web/wishlists/count/wishlist');
            $response_count_cart = $controller->api_get('web/carts/count/cart');
            $response_product_carts = $controller->api_get('web/carts/list');

            $cart_info = [];
            if($response_product_carts->data->list ?? null) {        
                $cart_info = $response_product_carts->data->list ?? null;
            }
            view()->share('cart_list', $cart_info ?? 0);
            view()->share('count_wishlist', $count_wishlist->data ?? 0);
            view()->share('count_cart', $response_count_cart->data->count ?? 0);
            view()->share('total', $response_count_cart->data->total ?? 0);
        }
        $response_product_categories = $controller->api_get('web/product_categories/list');
        $response_products = $controller->api_get('web/products/list?limit=50');
        $product_categories = [];
        if($response_product_categories->data) {        
            $product_categories = $response_product_categories->data;
        }
        $products = [];
        if($response_products->data->list) {        
            $products = $response_products->data->list;
        }

        $count = 0;
        foreach ($products as $index => $list) {
            if($list->is_new == 1){
                $count++;
            }

        }
        
        view()->share('product_categories', $product_categories ?? 0);
        view()->share('count', $count ?? 0);
    }

    function getLocaleUrl($url)
    {
        if(app()->getLocale() == 'en')
            return url($url);

        return url( app()->getLocale() . '/' . $url);
    }

    function ok($data = null)
    {
        return response()->json(['success' => true, 'data' => $data]);
    }

    function fail($message, $code = 400)
    {
        return response()->json(["success" => false, "message" => $message], $code);
    }

    function formatCurrency($money, $code = 'usd')
    {
        switch ($code) {
            case 'riel':
                return number_format($money, 0, '', ',') . '';
            default:
                return '$' . number_format($money, 2, '.', ',');
        }
    }