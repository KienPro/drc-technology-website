<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function registerIndex()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $password = $request->password;
        $cfpassword = $request->cfpassword;
        if($password == $cfpassword){
            $result = $this->api_post('web/auth/register', $request->all());
            if ($result->success == false) {
                return back()->withInput(request()->except('password'))->with('error', self::getErrorMessage($result->message));
            }
        }else{
            return back()->withInput()->with('error', self::getErrorMessage("Password and confirm password not match!"));
        }
        
        
        session()->put('auth', $result->data);
        return redirect()->to('/home');
    }

    public function loginIndex()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $result = $this->api_post('web/auth/login', $request->all());
        if ($result->success == false) {
            return back()->withInput(request()->except('password'))->with('error', self::getErrorMessage($result->message));
        }
        session()->put('auth', $result->data);
        return redirect()->to('/home')->with('success','You log in successfully');;
    }    

    public function logout()
    {
        return self::clearAuth();
    }

    public function forget()
    {
        return view('auth.forget');
    }

    public function submitForgetPassword(Request $request)
    {
        $result = $this->api_post('web/auth/forgot_password', $request->all());
        if ($result->success == false) {
            return back()->withInput()->with('error', self::getErrorMessage($result->message));
        }

        return redirect()->to('auth/verify/'.$request->email);
    }

    public function verify($email)
    {
        return view('auth.verify', compact('email'));
    }

    public function submitVerify(Request $request)
    {
        $result = $this->api_post('web/auth/forgot_password/verify',$request->all());
        if ($result->success == false) {
            return back()->withInput()->with('error', self::getErrorMessage($result->message));
        }
        $email = $request->email;
        $token = $result->data->token;

        return redirect()->to('auth/reset/'.$email.'/'.$token);
    }

    public function reset($email, $token)
    {
        return view('auth.reset', compact('email', 'token'));
    }

    public function submitResetPassword(Request $request)
    {
        $password = $request->password;
        $confirm_password = $request->confirm_password;
        if($password == $confirm_password && $password!=null&$confirm_password!=null){
            $result = $this->api_post('web/auth/forgot_password/reset', $request->all());  
            if ($result->success == false) {
                return back()->withInput()->with('error', self::getErrorMessage($result->message));
            }
            return redirect()->to('/auth/login')->with('message', 'Password Reset Successfully');
        }elseif($password != $confirm_password){
            return back()->withInput()->with('error', self::getErrorMessage("Password and confirm password not match!"));
        }else{
            return back()->withInput()->with('error', self::getErrorMessage("Password and confirm password invalid!"));
        }

    }
}
