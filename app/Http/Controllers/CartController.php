<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        return view('cart');
    }

    public function cartData()
    {
        $response_cart = $this->api_get('web/carts/list');
        $carts = [];
        if($response_cart->data) {
            $carts = $response_cart->data;
        } 
        return ok($carts);
    }

    public function singleCart(Request $request)
    {
        $result = $this->api_post('web/carts/add/single_cart', $request->all());
        if ($result->success == true) {
            return ok('');
        }
    }

    public function multipleCart(Request $request)
    {
        $result = $this->api_post('web/carts/add/multi_cart', $request->all());
        if ($result->success == true) {
            session()->put('success', 'Order successfully');
            return redirect()->route("view-carts");
        }
    }

    public function delete($id)
    {
        $result = $this->api_post('web/carts/delete/'. $id);
        if ($result->success == true) {
            session()->put('success', 'Order successfully');
            return ok();
        } 
    }
}
