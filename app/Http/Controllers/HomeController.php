<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $response_categories = $this->api_get('web/product_categories/list');
        $response_slideshows = $this->api_get('web/slideshows/list');
        $response_product_tags = $this->api_get('web/product_tags/list');
        $response_product_bestsellers = $this->api_get('web/products/list?is_bestseller=1');

        $slideshows = [];
        if($response_slideshows->data) {        
            $slideshows = $response_slideshows->data;
        }

        $product_categories = [];
        if($response_categories->data) {
            $product_categories = $response_categories->data;
        } 

        $product_tags = [];
        if($response_product_tags->data) {
            $product_tags = $response_product_tags->data;
        } 
        
        $product_bestsellers = [];
        if($response_product_bestsellers->data->list) {
            $product_bestsellers = $response_product_bestsellers->data->list;
        } 
        return view('home.index', compact('product_categories', 'slideshows', 'product_tags', 'product_bestsellers'));
    }

    public function product()
    {
        $offset = request('offset');
        $search = request('search');
        $is_new = request('is_new');
        $response_products = $this->api_get('web/products/list?offset='. $offset. '&search='.$search. '&is_new='.$is_new);
        $products = [];
        if($response_products->data){
            $products = $response_products->data;
        }
        return ok($products);
    }

    public function quickView($id)
    {
        $response_product_quickview = $this->api_get('web/products/quickview/'. $id);
        $product_quickview = [];
        if($response_product_quickview->data) {
            $product_quickview = $response_product_quickview->data;
        } 
        return ok($product_quickview);
    }

    public function compare($id)
    {
        $response_product_compare = $this->api_get('web/products/compare/'. $id);
        $product_compare = [];
        if($response_product_compare->data) {
            $product_compare = $response_product_compare->data;
        } 
        return ok($product_compare);
    }

    public function productDetails($id)
    {
        $response_product_details = $this->api_get('web/products/details/'. $id);
        $response_related_products = $this->api_get('web/products/list?product_category_id='. $response_product_details->data->category->id);
        $product_details = [];
        if($response_product_details->data) {
            $product_details = $response_product_details->data;
        } 
        $related_products = [];
        if($response_related_products->data->list) {
            $related_products = $response_related_products->data->list;
        } 
        return view('home.product_detail', compact('product_details', 'related_products'));
    }
}
