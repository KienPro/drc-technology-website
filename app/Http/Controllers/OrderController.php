<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function store(Request $request)
    {       
        $result = $this->api_post('web/orders/create', $request->all());
        if ($result->success == true) {
            session()->put('success', 'Order successfully');
            return ok('');
        } else {
            return fail($result->message, 200);
        } 
    }
}
