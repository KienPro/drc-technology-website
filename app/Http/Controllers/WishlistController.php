<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function index()
    {
        list($current_page, $limit, $offset, $search, $order, $sort) = $this->getParams();

        $data = $this->pagination(
            'web/wishlists/list',
            $limit,
            $offset,
            $search,
            $order,
            $sort,
            url('/wishlist'),
            $current_page
        );    
        return view('wishlist', compact('data'));
    }

    public function wish(Request $request)
    {
        $result = $this->api_post('web/wishlists/wish', $request->all());
        if ($result->success == true) {
            return ok('');
        }
    }

    public function unwish(Request $request, $id)
    {
        $result = $this->api_post('web/wishlists/unwish/'.$id, $request->all());
        if($result->success == true){
            session()->put('success', 'Product removed successfully');            
        } else {
            session()->put('error', self::getErrorMessage($result->message));
        }
        return back();
    }
}
