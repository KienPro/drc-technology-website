<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function edit()
    {
        $response = $this->api_get('web/auth/profile');
        $data = null;
        if ($response->data) {
             $data = $response->data;
        }
        return view('profile', compact('data'));
    }

    public function update(Request $request)
    {
        $result = $this->api_post('web/auth/profile', $request->all());
        if ($result->success == true) {
            session()->put('success', 'Your profile updated successfully!');
        } else {
            session()->put('error', self::getErrorMessage($result->message));
        }
        return back();
    }

    public function changepassword(Request $request)
    {
        $password = $request->password;
        $confirm_password = $request->confirm_password;
        if($password === $confirm_password){
            $result = $this->api_post('web/auth/profile/password', $request->all());
            if ($result->success == true) {
                session()->put('success', 'Your profile updated successfully!');
            }else{
                session()->put('error', self::getErrorMessage($result->message));
            }
        } else {
            session()->put('error', 'Password and Confirm Password not match');
        }
        
        return back();
    }

}
