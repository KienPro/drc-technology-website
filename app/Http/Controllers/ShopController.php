<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index(){
        $response_categories = $this->api_get('web/product_categories/list');
        $response_slideshows = $this->api_get('web/slideshows/list');
        $response_product_tags = $this->api_get('web/product_tags/list');
        $response_product_bestsellers = $this->api_get('web/products/list?is_bestseller=1');

        $slideshows = [];
        if($response_slideshows->data) {        
            $slideshows = $response_slideshows->data;
        }

        $product_categories = [];
        if($response_categories->data) {
            $product_categories = $response_categories->data;
        } 

        $product_tags = [];
        if($response_product_tags->data) {
            $product_tags = $response_product_tags->data;
        } 
        
        $product_bestsellers = [];
        if($response_product_bestsellers->data->list) {
            $product_bestsellers = $response_product_bestsellers->data->list;
        } 
        return view('shop', compact('product_categories', 'slideshows', 'product_tags', 'product_bestsellers'));
    }
}
