const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/dist/js')
    .sass('resources/sass/app.scss', 'public/dist/css')

    .js('resources/js/home.js', 'public/dist/js/home')
    .js('resources/js/product_details.js', 'public/dist/js/product_details')
    .js('resources/js/compare.js', 'public/dist/js/compare')
    .js('resources/js/wishlist.js', 'public/dist/js/wishlist')
    .js('resources/js/cart.js', 'public/dist/js/cart')
